package questions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mark on 18/05/2017.
  */
object BasicRDDApp extends App{
  val conf = new SparkConf().setAppName("Basic")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)





  /**
    10%
    example:
      input: List(1,2,3,4)
      output: Array(1,4,9,16)
    */
  def squares(nums:List[Int]): Array[Int] ={
    val rdd=sc.parallelize(nums)
   rdd.map(nums=>nums*nums).collect()
  }
  /**
    10%
    example:
      input: List(1,2,3,4)
      output: Array(1,3)
    */
  def odds(nums:List[Int]): Array[Int]= {
    val rdd=sc.parallelize(nums).map(nums=>{
      if (nums%2==1) ","+nums
      else
    }).collect()

  }

  /**
  10%
    使用mapValues將所有value均加1
    example:
      input: RDD((odd,1),(even,2),(odd,3),...,(even,100))
      output: RDD((odd,2),(even,3),(odd,4)...,(even,101))
    */
  def keyValueRdd:RDD[(String,Int)]={
    val kvRdd=sc.parallelize(1 to 100).map(v=>{
      if (v%2==0) "even"->v else "odd"->v
    })
    kvRdd.mapValues(_=>(String,v+1)).take(10).foreach(println)
  }



}
