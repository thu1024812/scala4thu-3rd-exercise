package questions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  30%
  請使用sc.textFile讀取路徑dataset/hello.txt，並計算WordCount
  輸出結果為
  world,2
  Hello,2
  are,1
  hello,1
  how,1
  you,1

  */

object WordCountApp {
  val conf = new SparkConf().setAppName("WordCount")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)


  val lines: RDD[String] =sc.textFile("dataset/hello.txt")
  val words: RDD[String] = lines.flatMap(split(" "))


  val wordCount:RDD[(String,Int)]={
    wordCount.map(_=>(words,1)).reduceByKey((acc,rnd)=>(acc+rnd))
  }


  wordCount.foreach(println)



}
